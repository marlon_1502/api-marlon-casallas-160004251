<?php

   include "conexion.php";
   $contenido = $dbh->query( "SELECT * FROM BasesLab1");
?>

<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <title>Laboratorio</title>
      <link rel="stylesheet" href="tabla.php" type="text/css">
   </head>
   <body>   
      <h1>Lista de Estudiantes</h1>   
      <table style="width: 60%">       
         <tr>
            <th>#</th>
            <th>Nombre</th>
            <th>Codigo</th>
            <th>Edad</th>
            <th>Correo</th>
            <th>Semestre</th>               
         </tr>
         <tbody>
            <?php foreach($contenido as $i=>$table){?>
               <tr>                         
                  <td><?php echo $table['id'] ?></td>
                  <td><?php echo $table['nombre'] ?></td>
                  <td><?php echo $table['codigo'] ?></td>
                  <td><?php echo $table['edad'] ?></td>
                  <td><?php echo $table['correo'] ?></td>
                  <td><?php echo $table['semestre'] ?></td>                           
               </tr>
            <?php } ?>
         </tbody>
      </table>            
   </body>
</html>
