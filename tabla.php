<?php 

	header("Content-Type: text/css; charset: UTF-8 ");

?>

body{
	margin: 50px; 
   	width: 1000px; 
}

table, th, td{
	border: 5px solid white;
    border-collapse: collapse;
    text-align: left;
}

th{
	background-color: #246355;        
    color: white;
}
th, td{
	padding: 10px;
}

thead{
	background-color: #246355;
	border-bottom: solid 5px #0F362D;
	color: white;
}

tr:nth-child(even){
	background-color: #ddd;
}


